Please acknowledge the facility for the support received and its staff :
- in your posters and presentations
- in your publications 
according to the following model for the Imaging plateform and press on the icon below to know the modalities for the image analysis:

 **_“We acknowledge the support of the Light Microscopy Facility at the IGBMC imaging center, member of the national infrastructure France-BioImaging supported by the French National Research Agency (ANR-10-INBS-04)”_**
