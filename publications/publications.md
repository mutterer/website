## Reporting your Publications ##
The light microscopy core facility does apply for various grants (IBiSA, UNISTRA, CPER, La Ligue etc.) or is a co-applicant (ANR, APP, FRC, etc.) - representing 792 k euros of investment from 2019 to 2023 - and is also audited by the GIS IBISA, CORTECS UNISTRA, HCERES, etc. . 

The facility needs to demonstrate its active participation to the scientific output of the IGBMC to all these funders/auditors, and for that I do need an exhaustive list of the scientific output  i.e. the publications where the instruments/staff expertise of the facility was used.

You can easily report your publications directly on [PPMS](https://ppms.eu/igbmc/pub/?pf=3)

Thank you very much for your help.


## Selected Publications ##

1. Cytosolic sequestration of the vitamin D receptor as a therapeutic option for vitamin D-induced hypercalcemia. 
Rovito D, Belorusova AY, Chalhoub S, Rerra AI, Guiot E, Molin A, Linglart A, Rochel N, Laverny G, Metzger D.
Nat Commun. 2020 Dec 7;11(1):6249. doi: 10.1038/s41467-020-20069-4.
PMID: 33288743 Free PMC article.

2. Comprehensive integrative profiling of upper tract urothelial carcinomas. 
Su X, Lu X, Bazai SK, Compérat E, Mouawad R, Yao H, Rouprêt M, Spano JP, Khayat D, Davidson I, Tannir NN, Yan F, Malouf GG.
Genome Biol. 2021 Jan 4;22(1):7. doi: 10.1186/s13059-020-02230-w.
PMID: 33397444 Free PMC article.

3. Translation of GGC repeat expansions into a toxic polyglycine protein in NIID defines a novel class of human genetic disorders: The polyG diseases. 
Boivin M, Deng J, Pfister V, Grandgirard E, Oulad-Abdelghani M, Morlet B, Ruffenach F, Negroni L, Koebel P, Jacob H, Riet F, Dijkstra AA, McFadden K, Clayton WA, Hong D, Miyahara H, Iwasaki Y, Sone J, Wang Z, Charlet-Berguerand N.
Neuron. 2021 Jun 2;109(11):1825-1835.e5. doi: 10.1016/j.neuron.2021.03.038. Epub 2021 Apr 21.
PMID: 33887199 Free PMC article.

4. FFAT motif phosphorylation controls formation and lipid transfer function of inter-organelle contacts. 
Di Mattia T, Martinet A, Ikhlef S, McEwen AG, Nominé Y, Wendling C, Poussin-Courmontagne P, Voilquin L, Eberling P, Ruffenach F, Cavarelli J, Slee J, Levine TP, Drin G, Tomasetto C, Alpy F.
EMBO J. 2020 Dec 1;39(23):e104369. doi: 10.15252/embj.2019104369. Epub 2020 Oct 30.
PMID: 33124732 Free PMC article.

5. SCA7 Mouse Cerebellar Pathology Reveals Preferential Downregulation of Key Purkinje Cell-Identity Genes and Shared Disease Signature with SCA1 and SCA2. 
Niewiadomska-Cimicka A, Doussau F, Perot JB, Roux MJ, Keime C, Hache A, Piguet F, Novati A, Weber C, Yalcin B, Meziane H, Champy MF, Grandgirard E, Karam A, Messaddeq N, Eisenmann A, Brouillet E, Nguyen HHP, Flament J, Isope P, Trottier Y. J Neurosci. 2021 Jun 2;41(22):4910-4936. doi: 10.1523/JNEUROSCI.1882-20.2021. Epub 2021 Apr 22.
PMID: 33888607 Free PMC article.

6. A PKD-MFF signaling axis couples mitochondrial fission to mitotic progression. 
Pangou E, Bielska O, Guerber L, Schmucker S, Agote-Arán A, Ye T, Liao Y, Puig-Gamez M, Grandgirard E, Kleiss C, Liu Y, Compe E, Zhang Z, Aebersold R, Ricci R, Sumara I.
Cell Rep. 2021 May 18;35(7):109129. doi: 10.1016/j.celrep.2021.109129.
PMID: 34010649

7. Activation of homologous recombination in G1 preserves centromeric integrity. 
Yilmaz D, Furst A, Meaburn K, Lezaja A, Wen Y, Altmeyer M, Reina-San-Martin B, Soutoglou E.
Nature. 2021 Dec;600(7890):748-753. doi: 10.1038/s41586-021-04200-z. Epub 2021 Dec 1.
PMID: 34853474

8. Deubiquitylase UCHL3 regulates bi-orientation and segregation of chromosomes during mitosis. 
Jerabkova K, Liao Y, Kleiss C, Fournane S, Durik M, Agote-Arán A, Brino L, Sedlacek R, Sumara I.
FASEB J. 2020 Sep;34(9):12751-12767. doi: 10.1096/fj.202000769R. Epub 2020 Aug 1.
PMID: 32738097

9. Toward a Consensus in the Repertoire of Hemocytes Identified in Drosophila. 
Cattenoz PB, Monticelli S, Pavlidaki A, Giangrande A.
Front Cell Dev Biol. 2021 Mar 4;9:643712. doi: 10.3389/fcell.2021.643712. eCollection 2021.
PMID: 33748138 Free PMC article.

10. Temporal specificity and heterogeneity of Drosophila immune cells. 
Cattenoz PB, Sakr R, Pavlidaki A, Delaporte C, Riba A, Molina N, Hariharan N, Mukherjee T, Giangrande A.
EMBO J. 2020 Jun 17;39(12):e104486. doi: 10.15252/embj.2020104486. Epub 2020 Mar 12.
PMID: 32162708 Free PMC article.

11. The Repo Homeodomain Transcription Factor Suppresses Hematopoiesis in Drosophila and Preserves the Glial Fate. 
Trébuchet G, Cattenoz PB, Zsámboki J, Mazaud D, Siekhaus DE, Fanto M, Giangrande A.
J Neurosci. 2019 Jan 9;39(2):238-255. doi: 10.1523/JNEUROSCI.1059-18.2018. Epub 2018 Nov 30.
PMID: 30504274 Free PMC article.

12. Integrative approach to interpret DYRK1A variants, leading to a frequent neurodevelopmental disorder.
Courraud J, Chater-Diehl E, Durand B, Vincent M, Del Mar Muniz Moreno M, Boujelbene I, Drouot N, Genschik L, Schaefer E, Nizon M, Gerard B, Abramowicz M, Cogné B, Bronicki L, Burglen L, Barth M, Charles P, Colin E, Coubes C, David A, Delobel B, Demurger F, Passemard S, Denommé AS, Faivre L, Feger C, Fradin M, Francannet C, Genevieve D, Goldenberg A, Guerrot AM, Isidor B, Johannesen KM, Keren B, Kibæk M, Kuentz P, Mathieu-Dramard M, Demeer B, Metreau J, Steensbjerre Møller R, Moutton S, Pasquier L, Pilekær Sørensen K, Perrin L, Renaud M, Saugier P, Rio M, Svane J, Thevenon J, Tran Mau Them F, Tronhjem CE, Vitobello A, Layet V, Auvin S, Khachnaoui K, Birling MC, Drunat S, Bayat A, Dubourg C, El Chehadeh S, Fagerberg C, Mignot C, Guipponi M, Bienvenu T, Herault Y, Thompson J, Willems M, Mandel JL, Weksberg R, Piton A.
Genet Med. 2021 Nov;23(11):2150-2159. doi: 10.1038/s41436-021-01263-1. Epub 2021 Aug 3.
PMID: 34345024

13. De Novo Frameshift Variants in the Neuronal Splicing Factor NOVA2 Result in a Common C-Terminal Extension and Cause a Severe Form of Neurodevelopmental Disorder. 
Mattioli F, Hayot G, Drouot N, Isidor B, Courraud J, Hinckelmann MV, Mau-Them FT, Sellier C, Goldman A, Telegrafi A, Boughton A, Gamble C, Moutton S, Quartier A, Jean N, Van Ness P, Grotto S, Nambot S, Douglas G, Si YC, Chelly J, Shad Z, Kaplan E, Dineen R, Golzio C, Charlet-Berguerand N, Mandel JL, Piton A.
Am J Hum Genet. 2020 Apr 2;106(4):438-452. doi: 10.1016/j.ajhg.2020.02.013. Epub 2020 Mar 19.
PMID: 32197073 Free PMC article.

14. Novel mutations in NLGN3 causing autism spectrum disorder and cognitive impairment.
Quartier A, Courraud J, Thi Ha T, McGillivray G, Isidor B, Rose K, Drouot N, Savidan MA, Feger C, Jagline H, Chelly J, Shaw M, Laumonnier F, Gecz J, Mandel JL, Piton A.
Hum Mutat. 2019 Nov;40(11):2021-2032. doi: 10.1002/humu.23836. Epub 2019 Jul 29.
PMID: 31184401

15. Single-cell analyses unravel cell type-specific responses to a vitamin D analog in prostatic precancerous lesions.
Abu El Maaty MA, Grelet E, Keime C, Rerra AI, Gantzer J, Emprou C, Terzic J, Lutzing R, Bornert JM, Laverny G, Metzger D.
Sci Adv. 2021 Jul 30;7(31):eabg5982. doi: 10.1126/sciadv.abg5982. Print 2021 Jul.
PMID: 34330705 Free PMC article.

16. The structure of the mouse ADAT2/ADAT3 complex reveals the molecular basis for mammalian tRNA wobble adenosine-to-inosine deamination. 
Ramos-Morales E, Bayam E, Del-Pozo-Rodríguez J, Salinas-Giegé T, Marek M, Tilly P, Wolff P, Troesch E, Ennifar E, Drouard L, Godin JD, Romier C.
Nucleic Acids Res. 2021 Jun 21;49(11):6529-6548. doi: 10.1093/nar/gkab436.
PMID: 34057470 Free PMC article.

17. Dyrk1a gene dosage in glutamatergic neurons has key effects in cognitive deficits observed in mouse models of MRD7 and Down syndrome. 
Brault V, Nguyen TL, Flores-Gutiérrez J, Iacono G, Birling MC, Lalanne V, Meziane H, Manousopoulou A, Pavlovic G, Lindner L, Selloum M, Sorg T, Yu E, Garbis SD, Hérault Y.
PLoS Genet. 2021 Sep 29;17(9):e1009777. doi: 10.1371/journal.pgen.1009777. eCollection 2021 Sep.
PMID: 34587162 Free PMC article.

18. Anterior-enriched filopodia create the appearance of asymmetric membrane microdomains in polarizing C. elegans zygotes. 
Hirani N, Illukkumbura R, Bland T, Mathonnet G, Suhner D, Reymann AC, Goehring NW.
J Cell Sci. 2019 Jul 15;132(14):jcs230714. doi: 10.1242/jcs.230714.
PMID: 31221727 Free PMC article.

19. Further delineation of KIF21B-related neurodevelopmental disorders.
Narayanan DL, Rivera Alvarez J, Tilly P, do Rosario MC, Bhat V, Godin JD, Shukla A.
J Hum Genet. 2022 Dec;67(12):729-733. doi: 10.1038/s10038-022-01087-0. Epub 2022 Oct 6.
PMID: 36198761

20. Native holdup (nHU) to measure binding affinities from cell extracts. 
Zambo B, Morlet B, Negroni L, Trave G, Gogl G.
Sci Adv. 2022 Dec 21;8(51):eade3828. doi: 10.1126/sciadv.ade3828. Epub 2022 Dec 21.
PMID: 36542723 Free PMC article.

21. Gut Microbiota Remodeling and Intestinal Adaptation to Lipid Malabsorption After Enteroendocrine Cell Loss in Adult Mice. 
Blot F, Marchix J, Ejarque M, Jimenez S, Meunier A, Keime C, Trottier C, Croyal M, Lapp C, Mahe MM, De Arcangelis A, Gradwohl G.
Cell Mol Gastroenterol Hepatol. 2023;15(6):1443-1461. doi: 10.1016/j.jcmgh.2023.02.013. Epub 2023 Feb 28.
PMID: 36858136 Free PMC article.

22. Tamoxifen improves muscle structure and function of Bin1- and Dnm2-related centronuclear myopathies. 
Gineste C, Simon A, Braun M, Reiss D, Laporte J.
Brain. 2023 Jul 3;146(7):3029-3048. doi: 10.1093/brain/awac489.
PMID: 36562127

23. Hierarchical TAF1-dependent co-translational assembly of the basal transcription factor TFIID. 
Bernardini A, Mukherjee P, Scheer E, Kamenova I, Antonova S, Mendoza Sanchez PK, Yayli G, Morlet B, Timmers HTM, Tora L.
Nat Struct Mol Biol. 2023 Aug;30(8):1141-1152. doi: 10.1038/s41594-023-01026-3. Epub 2023 Jun 29.
PMID: 37386215 Free PMC article.

24. MTM1 overexpression prevents and reverts BIN1-related centronuclear myopathy.
Giraud Q, Spiegelhalter C, Messaddeq N, Laporte J.
Brain. 2023 Oct 3;146(10):4158-4173. doi: 10.1093/brain/awad251.
PMID: 37490306 Free PMC article.

25. Intracellular and Plasma Membrane Cholesterol Labeling and Quantification Using Filipin and GFP-D4.
Wilhelm LP, Voilquin L, Kobayashi T, Tomasetto C, Alpy F.
Methods Mol Biol. 2019;1949:137-152. doi: 10.1007/978-1-4939-9136-5_11.
PMID: 30790254

26. Identification of MOSPD2, a novel scaffold for endoplasmic reticulum membrane contact sites. 
Di Mattia T, Wilhelm LP, Ikhlef S, Wendling C, Spehner D, Nominé Y, Giordano F, Mathelin C, Drin G, Tomasetto C, Alpy F.
EMBO Rep. 2018 Jul;19(7):e45453. doi: 10.15252/embr.201745453. Epub 2018 Jun 1.
PMID: 29858488 Free PMC article.

27. Distinct mechanisms underlie H2O2 sensing in C. elegans head and tail. 
Quintin S, Aspert T, Ye T, Charvin G. PLoS One. 2022 Sep 29;17(9):e0274226. doi: 10.1371/journal.pone.0274226. eCollection 2022.
PMID: 36173997 Free PMC article.

28. Left-right asymmetry in oxidative stress sensing neurons in C. elegans. 
Quintin S, Charvin G. MicroPubl Biol. 2022 Oct 19;2022:10.17912/micropub.biology.000652. doi: 10.17912/micropub.biology.000652. eCollection 2022.
PMID: 36338152 Free PMC article.

29. ATAC and SAGA co-activator complexes utilize co-translational assembly, but their cellular localization properties and functions are distinct. 
Yayli G, Bernardini A, Mendoza Sanchez PK, Scheer E, Damilot M, Essabri K, Morlet B, Negroni L, Vincent SD, Timmers HTM, Tora L.
Cell Rep. 2023 Sep 26;42(9):113099. doi: 10.1016/j.celrep.2023.113099. Epub 2023 Sep 8.
PMID: 37682711 Free PMC article.
30. AimSeg: A machine-learning-aided tool for axon, inner tongue and myelin segmentation. 
Carrillo-Barberà P, Rondelli AM, Morante-Redolat JM, Vernay B, Williams A, Bankhead P.
PLoS Comput Biol. 2023 Nov 17;19(11):e1010845. doi: 10.1371/journal.pcbi.1010845. eCollection 2023 Nov.
PMID: 37976310 Free PMC article.

31. Aberrant induction of p19Arf-mediated cellular senescence contributes to neurodevelopmental defects. 
Rhinn M, Zapata-Bodalo I, Klein A, Plassat JL, Knauer-Meyer T, Keyes WM.
PLoS Biol. 2022 Jun 14;20(6):e3001664. doi: 10.1371/journal.pbio.3001664. eCollection 2022 Jun.
PMID: 35700169 Free PMC article.

32. Gcm counteracts Toll-induced inflammation and impacts hemocyte number through cholinergic signaling. 
Bazzi W, Monticelli S, Delaporte C, Riet C, Giangrande A, Cattenoz PB.
Front Immunol. 2023 Nov 15;14:1293766. doi: 10.3389/fimmu.2023.1293766. eCollection 2023.
PMID: 38035083 Free PMC article.

33. An anti-inflammatory transcriptional cascade conserved from flies to humans. 
Pavlidaki A, Panic R, Monticelli S, Riet C, Yuasa Y, Cattenoz PB, Nait-Oumesmar B, Giangrande A.
Cell Rep. 2022 Oct 18;41(3):111506. doi: 10.1016/j.celrep.2022.111506.
PMID: 36261018